#!/usr/bin/env python3

# maybe temporary, currently get arguments
import sys
# process image with edge detection
from PIL import Image, ImageFilter, ImageStat
# list directory content
import os
# regex for page number search
import re

# this huge try is just to handle keyboard interrupts
try:
    # check single or double page
    def detect_orientation(path,imagessingle,imagesdouble):
        # open image
        image = Image.open(path)
        # get image dimensions
        width = image.size[0]
        height = image.size[1]
        if width/height > 1:
            imagesdouble.append(path)
        else:
            imagessingle.append(path)
        # close image
        image.close()
        return imagessingle,imagesdouble

    # use brightness based approach to determine pages to split
    def detect_brightness(path,imagesdouble,imagessplit):
        # open image
        image = Image.open(path)
        # get image dimensions
        width = image.size[0]
        height = image.size[1]
        # NOTE my checker complained about the "width/2" part without "int()"
        # crop left
        climage = image.crop((int(width/2-50),0,int(width/2),height))
        # crop right
        crimage = image.crop((int(width/2),0,int(width/2+50),height))
        # get same values from 0 to 1 as with the bash script
        # brightness left and brightness right
        brcl = ImageStat.Stat(climage).mean[0]/255
        brcr = ImageStat.Stat(crimage).mean[0]/255
        # close crop images again
        climage.close()
        crimage.close()
        # check against brightness threshold
        if brcl > 0.93 or brcr > 0.93:
            #print("split")
            imagessplit.append(path)
        # double pages that won't be split
        #else:
            #print("no split")
        # close image
        image.close()
        return imagesdouble,imagessplit

    # split pages and save separately
    def split_pages(path,output,num):
        # open image
        image = Image.open(path)
        # get image dimensions
        width = image.size[0]
        height = image.size[1]
        # crop image in two
        # NOTE my checker complained about the "width/2" part without "int()"
        leftimage = image.crop((0,0,int(width/2),height))
        rightimage = image.crop((int(width/2),0,width,height))
        # output paths for left and right page
        # TODO override for right to left comics
        leftimage.save(os.path.join(output,"page" + str(int(num)+1) + os.path.splitext(path)[1]))
        rightimage.save(os.path.join(output,"page" + num + os.path.splitext(path)[1]))
        # close images
        image.close()
        leftimage.close()
        rightimage.close()

    # parse the image's name
    def name_parser(path):
        # open image
        image = Image.open(path)
        # get image number (output as list type)
        # NOTE multiplte number in filename are presumed to not be a thing (only uses first number found)
        # OHGODTHEHORROR num[0] needs to be str(int(num[0])) to ensure equal removal of leading 0s for all output images
        num = str(int(re.findall('[0-9]+',os.path.basename(os.path.splitext(path)[0]))[0]))
        # get parent directory
        parent = os.path.dirname(path)
        # output directory name
        output = os.path.join(parent + "-cropped")
        # only create directory if it doesn't exist yet
        if not os.path.isdir(output):
            os.mkdir(output)
        return image,output,num

    # handle image output
    # TODO check before overwrite
    # TODO move most of the stuff currently in the for-loops into a function
    def output_images(imagessingle,imagesdouble,imagessplit,imagesdontsplit):
        for i in range(0,len(imagessingle)):
            path = imagessingle[i]
            image,output,num = name_parser(path)
            image.save(os.path.join(output,"page" + num + os.path.splitext(path)[1]))
            # close image
            image.close()
        for i in range(0,len(imagesdouble)):
            path = imagesdouble[i]
            image,output,num = name_parser(path)
            image.save(os.path.join(output,"page" + num + os.path.splitext(path)[1]))
            # close image
            image.close()
        for i in range(0,len(imagessplit)):
            path = imagessplit[i]
            image,output,num = name_parser(path)
            split_pages(imagessplit[i],output,num)
        for i in range(0,len(imagesdontsplit)):
            path = imagesdontsplit[i]
            image,output,num = name_parser(path)
            image.save(os.path.join(output,"page" + num + os.path.splitext(path)[1]))

    # function to find image edges
    # TODO make this find something
    def edgefinder(path,imagesdouble):
        # open image
        image = Image.open(path)
        # convert to grayscale (required by edge detection)
        image = image.convert("L")
        # detect edges in image
        image = image.filter(ImageFilter.FIND_EDGES)
        # get image dimensions
        width = image.size[0]
        height = image.size[1]
        # height after cut of 10 pixels height
        cutheight = int(height-10)
        # NOTE my checker complained about the "width/2" part without "int()"
        # NOTE I start with height 5 and end on "height-5" to remove the white outline and any shimmering around the whole image
        # crop left
        climage = image.crop((int(width/2-10),5,int(width/2),int(height-5)))
        clwidth = climage.size[0]
        #clheight = climage.size[1]
        clpixels = [[0]*clwidth for _ in range(height)]
        '''
        NOTE
        This list will look like this:
        [[255, 234, 253, 4, 7, 1, 2, 3, 65, 92], [255, 234, 253, 4, 7, 1, 2, 3, 65, 92], ...]
        it's to be interpreted this way:
        [[255, 234, 253, 4, 7, 1, 2, 3, 65, 92],
         [255, 234, 253, 4, 7, 1, 2, 3, 65, 92],
         ...]
        ie. the first [] is the y-coordinate, the second [] the x-coordinate
        '''
        # crop right
        crimage = image.crop((int(width/2),5,int(width/2+10),int(height-5)))
        crwidth = crimage.size[0]
        #crheight = crimage.size[1]
        crpixels = [[0]*crwidth for _ in range(cutheight)]
        # TODO replace clwidth and crwidth with cutwidth
        # fill lists with pixel brightness values
        for x in range(clwidth):
            for y in range(cutheight):
                clpixels[y][x] = climage.getpixel((x,y))
        for x in range(crwidth):
            for y in range(cutheight):
                crpixels[y][x] = crimage.getpixel((x,y))
        # print all values somewhat orderly
        # NOTE two different variants to pad the numbers used
        '''
        with open(path + "-raw.txt", "w") as file:
            for y in range(cutheight):
                for x in range(clwidth):
                    print(str(clpixels[y][x]).rjust(3,'0'),end = " ",file=file)
                    #print(f'{str(clpixels[y][x]):03}',end = " ")
                print("     ",end = "",file=file)
                for x in range(crwidth):
                    #print(str(crpixels[y][x]).rjust(3,'0'),end = " ")
                    print(f'{str(crpixels[y][x]):0>3}',end = " ",file=file)
                print("",file=file)
        '''
        # to view images generated
        #climage.save(path + "-left.jpg")
        #crimage.save(path + "-right.jpg")
        #listleft = []
        listtopright = []
        listmidright = []
        listbotright = []
        for y in range(cutheight):
            if int(clpixels[y][9]) > 230:
                #left = clpixels[y][9]
                try:
                    topright = crpixels[y-1][0]
                except IndexError:
                    topright = 0
                midright = crpixels[y][0]
                try:
                    botright = crpixels[y+1][0]
                except IndexError:
                    botright = 0
                # store values in
                #listleft.append(left)
                listtopright.append(topright)
                listmidright.append(midright)
                listbotright.append(botright)
                # to visualize
                '''
                print("   " + "     " + str(topright).rjust(3,'0'))
                print(str(left).rjust(3,'0') + "     " + str(midright).rjust(3,'0'))
                print("   " + "     " + str(botright).rjust(3,'0'))
                print()
                '''
        # loop over lists and try to find pages that can be split
        imagesdontsplit = []
        for i in range(len(listtopright)):
            # check if any values are above 230
            if listtopright[i] > 230 and listmidright[i] > 230 and listbotright[i] > 230:
                # NOTE because we're calling this a few times, the same path will be present multiple times
                imagesdontsplit.append(path)
                # break out of loop as soon as we found a match
                break
        '''
        TODO compare left and right page
        this example should be detected as continuous:
        030 062 094 013 098 255 168 255 255 127      000 000 000 000 000 000 255 255 255 255
        000 000 000 000 000 024 018 000 000 255      211 255 255 037 000 000 000 000 000 072
        030 033 041 102 086 000 040 076 032 015      133 094 090 170 255 255 255 000 000 000

        check left side first for any value greater than 230 (subject to change) in the last column
        if one is found, check the first column on the right side, one row up, the same row and one row down
        if any of those values are over 230, we don't split the image!
        visual:
         ?   -> 230
        240  -> 127
         ?   -> 80
        '''
        '''
        Alternative method:
        one single cut down the middle with ~5 pixels on each side
        compare the pixels in column 5 and 6 as suggested above
        '''
        # close image
        image.close()
        return imagesdouble,imagesdontsplit

    # main entry function
    # NOTE images is a list-type and contains the full path to single images
    def cleaver(images):
        # list for single pages
        imagessingle = []
        # list for double pages
        imagesdouble = []
        # list for images that need to be split
        imagessplit = []
        # list for images not to split
        imagesdontsplit = []

        # NOTE check for orientation of images
        # REVIEW modify range to go through whole list later
        print("Checking images")
        #for i in range(1,5):
        for i in range(0,len(images)):
            path = images[i]
            imagessingle,imagesdouble = detect_orientation(path,imagessingle,imagesdouble)
        # remove duplicates
        imagesdouble = list(set(imagesdouble))
        imagessplit = list(set(imagessplit))

        print("Determine need for splitting")

        # detection using brightness
        # NOTE this loop serves to determine whether images have to be split or not
        for i in range(0,len(imagesdouble)):
            path = imagesdouble[i]
            imagesdouble,imagessplit = detect_brightness(path,imagesdouble,imagessplit)
        # NOTE here we remove the images we know are going to be split from "imagesdouble"
        # NOTE "imagesdouble" could now be passed to another function to perform a different kind of detection operation
        imagesdouble = list(set(imagesdouble) - set(imagessplit))
        # remove duplicates
        imagesdouble = list(set(imagesdouble))
        imagessplit = list(set(imagessplit))

        '''
        # detection using edgefinder
        for i in range(0,len(imagesdouble)):
            path = imagesdouble[i]
            imagesdouble,imagesdontsplit = edgefinder(path,imagesdouble)
        imagesdouble = list(set(imagesdouble) - set(imagesdontsplit))
        #imagesdouble = list(set(imagesdouble) - set(imagessplit))
        # remove duplicates
        imagesdouble = list(set(imagesdouble))
        imagessplit = list(set(imagessplit))
        imagesdontsplit = list(set(imagesdontsplit))
        '''

        # NOTE implement other detection methods similar to the loop + stuff after above

        print("Writing images to folders")
        output_images(imagessingle,imagesdouble,imagessplit,imagesdontsplit)

    # get cli arguments
    # NOTE move to cli/gui
    arguments=sys.argv

    # TODO move these list into some function
    # this list will hold all images found
    images = []

    # loop through given arguments
    # NOTE move to cli/gui
    # NOTE arguments[0] is the script itself
    for i in range(1,len(arguments)):
        # check the path given
        path = arguments[i]
        # only continue if path given is a folder
        if os.path.isdir(path):
            # specify valid image extensions here
            valid_extensions = [".jpg",".jpeg",".png"]
            for file in os.listdir(path):
                # get file extension
                ext = os.path.splitext(file)[1]
                if ext.lower() in valid_extensions:
                    images.append(os.path.join(path,file))
        # output if a file is given
        else:
            print(path + " is not a folder")

    # sort images list
    # REVIEW is this necessary?
    images.sort()

    # main function
    cleaver(images)

except KeyboardInterrupt:
    print("Exiting program")
    # return default linux SIGINT code
    exit(130)
